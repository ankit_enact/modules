 <?php
 // Sends Push notification for iOS users
 function iOS($data, $devicetoken,$userId,$link) {
 	// echo $devicetoken = "81c132db2d978175e27e45a98798cec3635d33dbe749e4294bf08d41e040a397";
 	
		// $passphrase = "Lbim2201";
 	$passphrase = PEM_PASSWORD;
 	$deviceToken = $devicetoken;
 	$ctx = stream_context_create();
		// ck.pem is your certificate file
 	// stream_context_set_option($ctx, 'ssl', 'local_cert', 'apns-production.pem');
 	stream_context_set_option($ctx, 'ssl', 'local_cert', 'pushcert.pem');
 	stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
		// Open a connection to the APNS server
 	$fp = stream_socket_client(
 		// 'ssl://gateway.sandbox.push.apple.com:2195', $err,
 		'ssl://gateway.push.apple.com:2195', $err,
 		
 		$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
 	if (!$fp)
 		// exit("Failed to connect: $err $errstr" . PHP_EOL);
 		if($_POST['requestType'] == 'schedule'){
 			return("Failed to connect: $err $errstr" . PHP_EOL);
 		}else{
 			exit("Failed to connect: $err $errstr" . PHP_EOL);
 		}			
 		
		// Create the payload body
 		$body['aps'] = array(
 			'alert' => array(
 				'body' => $data['msg'],
 				'orderId' => $data['orderId'],
 			),
 			'sound' => 'goWashit_notification_sound.wav'
 		);
		// Encode the payload as JSON
 		$payload = json_encode($body);
		// Build the binary notification
 		$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
		// Send it to the server
 		$result = fwrite($fp, $msg, strlen($msg));
 		// print_r($msg);die;
		// Close the connection to the server
 		fclose($fp);
 		$pushMsg = $data['msg'];
 		$orderId = $data['orderId'];
 		if(!$result){
 			
 			$sqlPushQuery = mysqli_query($link,"insert into push_log(push_msg,user_id,order_id,device_token,delivery_msg)values('".$pushMsg."','".$userId."','".$orderId."','".$deviceToken."','fail')"); 

 		}else{
 			$sqlPushQuery = mysqli_query($link,"insert into push_log(push_msg,user_id,order_id,device_token,delivery_msg)values('".$pushMsg."','".$userId."','".$orderId."','".$deviceToken."','success')"); 
 		}
 		if (!$result)
 			return 'Message not delivered' . PHP_EOL;
 		else
 			return 'Message successfully delivered' . PHP_EOL;
	} // iOS

	function android($data, $reg_id) {
		
    #API access key from Google API's Console
		define( 'GOOGLE_API_KEY', 'AAAAKXUL99M:APA91bGyzvb3AdXE1HohflCxk8aqw9TDd4FEmQIG2votHmiemFvkVDTN8v65pgsUX7gCFvDSmuSAp6tMSWbdqR_eNO4k2uJ50hfUezXSiv-kXBFgwwI28nORZ6T9z4vBUw7xyNiR-xRS' );
        //$registrationIds = 'e44gXuCKtx8:APA91bH0X4YUm16_0dctauP91LYJLSS7fQnIWiFhOfQFuKdVcdtkkWwChYJKDJOmL0hiDweWUXeZ0miK5CRD6jCVmJBlOv4UZlNepS5eADmylZqMC7ABYgRkH8qJhZv7BI93a22-F4iz';
		
    #prep the bundle
		$msg = array
		(
			'body'  => $data['msg'],
			'orderId' => $data['orderId'],
            'icon'  => 'myicon',//Default Icon
            'sound' => 'mySound'//Default sound
        );

		$fields = array
		(
			'to'           => $reg_id,
			'notification' => $msg
		);
		
		
		$headers = array
		(
			'Authorization: key=' . GOOGLE_API_KEY,
			'Content-Type: application/json'
		);
    #Send Reponse To FireBase Server    
		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );

} //android


/*
 * 
 * function for sendPushNotification
 */
function sendPushNotification($token,$oId,$uId,$platform,$link){

	$request = request($_SERVER['REQUEST_METHOD']);

            //echo "here<pre>"; print_r($request); exit;
	
	if((isset($request['toUserId']) && $request['toUserId'] != "") && (isset($request['pushMsg']) && $request['pushMsg'] != "")){

        	  // echo "221------------------".$request['toUserId']; 
		$userId  =  $request['toUserId'];
		$message = $request['pushMsg'];
		$sqlGetToken  = mysqli_query($link,"select platform,deviceToken from installations where userId = '".$userId."'");
		if ($sqlGetToken != "" && mysqli_num_rows($sqlGetToken)>0){

			while($rowToken = mysqli_fetch_array($sqlGetToken)){

				if($rowToken != "") {
					$platform  = $rowToken['platform'];
					$deviceToken  = $rowToken['deviceToken'];
		            		// Message payload
					$msg_payload = array (
						'msg' => $message,
						'orderId' => "",
					);

					if($platform == 'iOS'){
						$result = iOS($msg_payload,$deviceToken,$userId,$link);		
					}else if($platform == 'Android'){
						$result = android($msg_payload,$deviceToken);		
					}else{}
					
				}
			}
			sendResponse(200, json_encode(array("status"=>200,"message" => $result)));
		}
	}else if((isset($request['toUserId']) && $request['toUserId'] == "") || (isset($request['pushMsg']) && $request['pushMsg'] == "")){

 				//echo "250-----------------elseif-".$request['toUserId']; exit;

		if($request['toUserId'] == ""){
			sendResponse(201, json_encode(array("status"=>"0","message"=> USERID_MISSING)));
		}
		if($request['pushMsg'] == ""){
			sendResponse(201, json_encode(array("status"=>"0","message"=> PUSH_MSG_ERROR)));
		}
	}else{

         		//echo 'here'.$uId.'</br>'; 

         		//echo "250-----------------else".$request['toUserId']; exit;
         		/*$msg_payload = array (
            	'msg' => PUSH_MSG,
            	'orderId' => $oId,
            	);
            	$deviceToken = $token;
            	$result = iOS($msg_payload,$deviceToken,$uId);	*/

            	$deviceToken    = $token;
            	$platform = $platform;
	    		// Message payload
            	$msg_payload = array (
            		'msg' => PUSH_MSG,
            		'orderId' => $oId,
            	);
            	
            	if($platform == 'iOS'){
            		$result = iOS($msg_payload,$deviceToken,$userId,$link);		
            	}else if($platform == 'Android'){
            		$result = android($msg_payload,$deviceToken,$link);		
            	}else{}

            }
            
        }